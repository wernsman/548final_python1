import argparse
import os
from shutil import copyfile
from collections import defaultdict
import re
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

NAME_STRING = "name"
THROUGHPUT_STRING = "[OTG][I][APPLICATION] TX throughput"
GOODPUT_STRING = "[OTG][I][APPLICATION] RX goodput"
LAT_MAX_STRING = "[OTG][I][APPLICATION] OWD MAX (one way)ms"
LAT_MIN_STRING = "[OTG][I][APPLICATION] OWD MIN (one way)ms"
SEARCH_LIST = [THROUGHPUT_STRING, GOODPUT_STRING, LAT_MAX_STRING, LAT_MIN_STRING]
SEARCH_LIST_readable = {THROUGHPUT_STRING : "throughput", GOODPUT_STRING : "goodput", LAT_MAX_STRING : "latency_max" , LAT_MIN_STRING : "latency_min"}
testType = ""


verboseFlag=False
oaisimPathString=""
confPathString=""
tempReplacePathString=""

def parse_to_lists(filename):
    vout("parsing test output...")

    d = defaultdict(list)

    with open(filename) as infile:
        for line in infile:
            for search_term in SEARCH_LIST:
                if search_term in line:
                    # Parse out number (only detects the first decimal number match)
                    results = re.findall("\d+\.\d+", line)
                    # Add to list
                    d[search_term].append(results[0])

    for name in SEARCH_LIST:
        print(str(d[name]))

    return d

def compute_average(input_list):
    avg = 0
    list_len = len(input_list)
    for number in input_list:
        avg += float(number)
        list_len -= 1
        if(list_len <= 0):
            break

    return avg / len(input_list)

def generate_report(result_list, report_file_name):

    for search_term in SEARCH_LIST:
        max_len = 0;
        h_str = ""
        f_str = ""
        for d in result_list:
            f_str += d[NAME_STRING] + ","
            for item in d[search_term]:
                f_str += str(item) + ","
            f_str += "\n"
            max_len = max(max_len, len(d[search_term]))

        # Generate header
        h_str += "Name,"
        for i in range(0, max_len):
            h_str += SEARCH_LIST_readable[search_term].capitalize() + str(i) + ","
        h_str += "\n"

        # Write to file
        with open(report_file_name + "_" + SEARCH_LIST_readable[search_term] + ".csv", "w+") as f:
            f.write(h_str + f_str)

def create_plots(x_axis_list, result_list, testname):
    global testType
        # Plot avgs across multiple runs
    for search_term in SEARCH_LIST:
        avg_list = []

        # Collect averages from each of the runs
        for result in result_list:
            avg_list.append(compute_average(result[search_term]))

        # Plot a basic line graph
        matplotlib.pyplot.clf()
        matplotlib.pyplot.plot(x_axis_list, avg_list)

        y_label = "Avg. UE Latency (ms)"
        y_limits = [0,200]

        if testType is "UE":
            matplotlib.pyplot.xlabel("UE")
        if testType is "lat":
            matplotlib.pyplot.xlabel("Send Frequency")
        if testType is "size":
            matplotlib.pyplot.xlabel("Packet Size")

        if search_term is THROUGHPUT_STRING or search_term is GOODPUT_STRING:
            y_label = "Avg. UE Bandwidth (Kbps)"
            y_limits = [0,1000]

        matplotlib.pyplot.ylabel(y_label)
        matplotlib.pyplot.ylim(y_limits)

        matplotlib.pyplot.title(SEARCH_LIST_readable[search_term].capitalize())
        matplotlib.pyplot.savefig(testname+"_figure_" + SEARCH_LIST_readable[search_term] + ".png")


#if verbose output messages to stdout
def vout(msg):
    if verboseFlag == True:
        print msg

def eout(msg):
    print "[OAISIMRUNNER ERROR] - " + msg + "\n"


def main():
    global verboseFlag
    global oaisimPathString
    global confPathString
    global tempReplacePathString

    parser = argparse.ArgumentParser(description='Tool to run many oaisim simulations and collect data')
    #optional params
    parser.add_argument('--startUE', '-su', type=int, help='starting number of UE\'s',default=1)
    parser.add_argument('--endUE', '-eu', type=int, help='ending number of UE\'s',default=5)
    parser.add_argument('--startLat', '-sl', type=int, help='starting number for latency',default=50)
    parser.add_argument('--endLat', '-el', type=int, help='ending number for latency',default=50)
    parser.add_argument('--startSize', '-ss', type=int, help='starting number for size',default=1000)
    parser.add_argument('--endSize', '-es', type=int, help='ending number for size',default=1000)
    parser.add_argument('--rateUE', '-ru', type=int, help='UE rate between tests',default=0)
    parser.add_argument('--rateLat', '-rl', type=int, help='Latency rate between tests',default=0)
    parser.add_argument('--rateSize', '-rs', type=int, help='size rate between tests',default=0)
    parser.add_argument('--emutime', '-et', type=int, help='emulation time in ms', default=10000)
    parser.add_argument('--flowtime', '-ft', type=int, help='duration of the traffic flow', default=8000)

    parser.add_argument('--verbose', '-v', help='size rate between tests', action="store_true")

    #reqd params
    required = parser.add_argument_group('required arguments')
    required.add_argument('-o', '--output', type=str, help='output filname for log an plot', required=True)
    required.add_argument('-r', '--root', type=str, help='the root of the openinterface5g repository', required=True)
    required.add_argument('-t', '--template', type=str, help='file that is used as a template to create the xml file used in tests', required=True)

    args = parser.parse_args()
    verboseFlag = args.verbose

    vout("args : "+str(args))
    #check that we have everything we need to run simulations
    runCheck(args)
    runTestSweep(args)



#check for that all the required things exist
def runCheck(args):
    global oaisimPathString
    global confPathString
    global tempReplacePathString
    global testType

    vout( "root dir: " + str(args.root))

    #oaisim
    oaisimPathString = str(args.root) + "/cmake_targets/oaisim_noS1_build_oai/build/oaisim_nos1"
    vout("oaisimPathString - "+ oaisimPathString )
    if os.path.isfile(oaisimPathString) == False:
        eout("could not find oaisim_nos1: " + oaisimPathString)
        exit()

    #conf
    confPathString = str(args.root) + "/targets/PROJECTS/GENERIC-LTE-EPC/CONF/enb.band7.generic.oaisim.local_no_mme.conf"
    vout("nfPathString - "+ confPathString )
    if os.path.isfile(confPathString) == False:
        eout("could not find conf: " + confPathString)
        exit()

    #file to be replaced by template
    tempReplacePathString = str(args.root) + "/targets/SIMU/EXAMPLES/OSD/WEBXML/template_22.xml"
    vout("tempReplacePathString - "+ tempReplacePathString )
    if os.path.isfile(tempReplacePathString) == False:
        eout("could not find template_22.xml: " + tempReplacePathString)
        exit()

    #TODO check that the template file with signatures for each var exists

    #TODO create an empty template_22.xml file in the oaisim_nos1 directory

    #TODO add a check to make sure only one var type has a rate
    if (args.rateUE + args.rateLat + args.rateSize) == 0:
        eout("NO rate for UE, Lat, or Size")
        exit()
    if args.rateUE != 0:
        testType = "UE"
        if (args.rateLat + args.rateSize) != 0:
            eout("Only one test variable can have a rate")
            exit()
    if args.rateLat != 0:
        testType = "lat"
        if (args.rateUE + args.rateSize) != 0:
            eout("Only one test variable can have a rate")
            exit()
    if args.rateSize != 0:
        testType = "size"
        if (args.rateLat + args.rateUE) != 0:
            eout("Only one test variable can have a rate")
            exit()

    #TODO check if environment variables were set
    #TODO this doesn't actually work
    if 'OPENAIR_HOME' in os.environ == False:
        eout("environment variable missing, source oaienv in the root dir")
        exit()
    if 'OPENAIR_DIR' in os.environ == False:
        eout("environment variable missing, source oaienv in the root dir")
        exit()
    if 'OPENAIR1_DIR' in os.environ == False:
        eout("environment variable missing, source oaienv in the root dir")
        exit()
    if 'OPENAIR2_DIR' in os.environ == False:
        eout("environment variable missing, source oaienv in the root dir")
        exit()
    if 'OPENAIR3_DIR' in os.environ == False:
        eout("environment variable missing, source oaienv in the root dir")
        exit()
    if 'OPENAIR_TARGETS' in os.environ == False:
        eout("environment variable missing, source oaienv in the root dir")
        exit()

    #TODO add check for the nas source thing too

def runSim(confPath, xmlPath, simPath, testname):
    #run test
    failCount = 0
    maxFailCount = 3
    while failCount < maxFailCount: 
        runString = simPath+" -a -F -O "+confPath+" -c template_22.xml > current_output_log.txt"
        vout("\n\nRUNNING...\n"+testname+" : "+runString+"\n\n")
        returnVal = os.system(runString)
        if returnVal != 0:
            eout("runSim returned a non 0 value on test "+testname)
            failCount += 1
        else:
            failCount = maxFailCount

    return returnVal

def runTestSweep(args):
    result_list = []
    x_axis_list = []

    #backup template
    backupOriginalTemplate(args)

    #init vars
    curUE = args.startUE
    curLat = args.startLat
    curSize = args.startSize


    #loop through incrementing by step
    while curUE <= args.endUE and curLat <= args.endLat and curSize <= args.endSize:
        #create template
        createTestTemplate(args.template, curUE, curLat, curSize, args.emutime, args.flowtime)
        #create testname
        testname = "test_"+str(curUE)+"_"+str(curLat)+"_"+str(curSize)
        #runsim
        if runSim(confPathString, tempReplacePathString,  oaisimPathString, testname) != 0:
            #bailout!!!
            vout("putting OG template_22.xml back to it't home")
            replaceOriginalTemplate(args)
            vout("generating logs..")
            create_plots(x_axis_list, result_list, args.output)
            generate_report(result_list, args.output)
            exit()
        #add output to roberts lists
        d = parse_to_lists("current_output_log.txt")
        d[NAME_STRING] = testname
        result_list.append(d)

        if (args.rateUE != 0):
            x_axis_list.append(curUE)
        elif (args.rateLat != 0):
            x_axis_list.append(curLat)
        elif (args.rateSize != 0):
            x_axis_list.append(curSize)

        #update test vars
        curUE += args.rateUE
        curLat += args.rateLat
        curSize += args.rateSize

    #reset the template to the backup made
    vout("putting OG template_22.xml back to it't home")
    replaceOriginalTemplate(args)

    #generate logs and graphs
    vout("generating logs..")
    create_plots(x_axis_list, result_list, args.output)
    generate_report(result_list, args.output)

def backupOriginalTemplate(args):
    #check for file for template to replace and make backup if found
    tempReplacePathString = str(args.root) + "/targets/SIMU/EXAMPLES/OSD/WEBXML/template_22.xml" #already checked to exist
    vout("moving "+tempReplacePathString+" to ./backup_template_22")
    copyfile(tempReplacePathString, "backup_template_22")

def replaceOriginalTemplate(args):
    tempReplacePathString = str(args.root) + "/targets/SIMU/EXAMPLES/OSD/WEBXML/template_22.xml" #already checked to exist
    vout("moving backup_template_22 back to "+tempReplacePathString)
    copyfile("backup_template_22", tempReplacePathString)

def createTestTemplate(tempPath,ue,lat,size,emutime,flow):
    #check for template file
    tempPathString = str(tempPath)
    if os.path.isfile(tempPathString) == False:
        eout("could not find template file: " + tempPathString)
        exit()
    vout("reading from template file "+tempPathString)
    #find and replace strings in the template
    with open(tempPathString, 'r') as file:
        filedata = file.read()

    filedata = filedata.replace('!UE!', str(ue)) #this should be a look up not hardcoded...
    filedata = filedata.replace('!LAT!', str(lat))
    filedata = filedata.replace('!SIZE!', str(size))
    filedata = filedata.replace('!EMUTIME!', str(emutime))
    filedata = filedata.replace('!FLOW!', str(flow))

    vout("writing changes to the \"to be copied\" file")

    with open('template_22.xml', 'w') as file:
        file.write(filedata)

    #move to correct file location (where its oaisim is AND where xml is actually read)
    copyfile("template_22.xml", tempReplacePathString)




if __name__ == "__main__":
    main()
