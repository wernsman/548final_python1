from collections import defaultdict
import re
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

NAME_STRING = "name"
THROUGHPUT_STRING = "[OTG][I][APPLICATION] TX throughput"
GOODPUT_STRING = "[OTG][I][APPLICATION] RX goodput"
LAT_MAX_STRING = "[OTG][I][APPLICATION] OWD MAX (one way)ms"
LAT_MIN_STRING = "[OTG][I][APPLICATION] OWD MIN (one way)ms"
SEARCH_LIST = [THROUGHPUT_STRING, GOODPUT_STRING, LAT_MAX_STRING, LAT_MIN_STRING]
SEARCH_LIST_readable = {THROUGHPUT_STRING : "throughput", GOODPUT_STRING : "goodput", LAT_MAX_STRING : "latency_max" , LAT_MIN_STRING : "latency_min"}

def parse_to_lists(filename):
    print("Begin")

    d = defaultdict(list)

    with open(filename) as infile:
        for line in infile:
            for search_term in SEARCH_LIST:
                if search_term in line:
                    # Parse out number (only detects the first decimal number match)
                    results = re.findall("\d+\.\d+", line)
                    # Add to list
                    d[search_term].append(results[0])

    for name in SEARCH_LIST:
        print(str(d[name]))

    return d

def compute_average(input_list):
    avg = 0
    list_len = len(input_list)
    for number in input_list:
        avg += float(number)
        list_len -= 1
        if(list_len <= 0):
            break

    return avg / len(input_list)

def generate_report(result_list, report_file_name):

    for search_term in SEARCH_LIST:
        max_len = 0;
        h_str = ""
        f_str = ""
        for d in result_list:
            f_str += d[NAME_STRING] + ","
            for item in d[search_term]:
                f_str += str(item) + ","
            f_str += "\n"
            max_len = max(max_len, len(d[search_term]))

        # Generate header
        h_str += "Name,"
        for i in range(0, max_len):
            h_str += SEARCH_LIST_readable[search_term].capitalize() + str(i) + ","
        h_str += "\n"

        # Write to file
        with open(report_file_name + "_" + SEARCH_LIST_readable[search_term] + ".csv", "w+") as f:
            f.write(h_str + f_str)

def create_plots(result_list):
        # Plot avgs across multiple runs
    for search_term in SEARCH_LIST:
        avg_list = []

        # Collect averages from each of the runs
        for result in result_list:
            avg_list.append(compute_average(result[search_term]))

        # Plot a basic line graph
        matplotlib.pyplot.clf()
        matplotlib.pyplot.plot(avg_list)
        matplotlib.pyplot.ylabel(search_term)
        matplotlib.pyplot.title(SEARCH_LIST_readable[search_term].capitalize())
        matplotlib.pyplot.savefig("figure_" + SEARCH_LIST_readable[search_term] + ".png")

def main():
    # Emulated running loop (REPLACE THIS WITH DANE CODE)
    result_list = []
    for i in range(0,3):
        # Generate XML and run test here (THESE ARE TEMPORARY NAMES)
        d = parse_to_lists("log_test." + str(i))
        d[NAME_STRING] = "name" + str(i)

        result_list.append(d)

    # generate_report(result_list, "report")
    create_plots(result_list)
    generate_report(result_list, "report")


if __name__ == "__main__":
    main()